/* 
 * CONSTANTS
 */
var T_F = 5;// Number of elements of the Flow Vector
var W = []; // Weights for 

// Fitness score constants
var K = 100.0; // Maximum value
var K2 = 1.0; // Constant to weigh the FitSum/T_F

/* 
 * GLOBAL VARIABLES
 */
var seq = []; // Original sequence elements
var originalseq = [] ; // Original sequence order
var N; // Size of original sequence
var distances = []; // Distances between elements  in the sequence
var vo = []; // "Flow" vector of original sequence

var otherseq = []; // other sequence
var vs = [];

function setup(){
	createCanvas(500,400);

	setupOriginalSequence();
	calculateDistances();
	setupScoreSystem();

	setupOtherSequence();
	vs = getFlowV(otherseq);





}


function draw(){
	background(25);
	stroke(0,255,0);
	fill(0,255,0);
	drawSequencePoints(originalseq,50,50,200,100);
	drawSequencePoints(otherseq,50,200,200,100);
	noStroke();
	text("Flow Vector <" + vo +">",280,50);
	text("Fitness score " + getFitScore(originalseq)  +"%",280,80);
	text("Flow Vector <" + vs +">",280,200);
	text("Fitness score " + getFitScore(otherseq)  +"%",280,230);

}

function setupOriginalSequence(){
	// "Load" sequence
	for(var i=0; i<100; i++){
//		seq[i] = floor(25*sin(i*TWO_PI*1.2/100)+25);
		seq[i] = floor(noise(i*0.1)*50);
//		seq[i] = i*1;
		originalseq[i] = i;
	}
	N = seq.length;
}

function setupOtherSequence(){
	// Random order
//	otherseq = shuffle(originalseq);
	
	for(var i=0; i<N*2; i++){
		// Reverse order
//		otherseq[i] = 100-i-1;
		// Same order
		otherseq[i] = i%N;
		// Same element
//		otherseq[i] = 0;
		// Random element
//		otherseq[i] = floor(random(N));
		// Half frequency element
//		otherseq[i] = floor(i/2);
//		otherseq[i] = floor(i/2);
		// Double frequency element
//		otherseq[i] = floor(i*2)%N;
		// Shifted element
		// Half
//		otherseq[i] = (i + N/2) % N;
//		otherseq[i] = (i%N + N/2) % N;
		// One element
//		otherseq[i] = (i + 1) % N;
//		otherseq[i] = (i%N + 1) % N;

	}
//	otherseq[0] = 25;
}

// Create table of distances
function calculateDistances(){
	for(var i=0; i<N; i++){
		distances[i] = [];
		for(var j=0; j<N; j++){
			distances[i][j] = abs(seq[i]-seq[j]);
		}
	}
	console.log(distances);
}



function setupScoreSystem(){
	// Initialize weights
	for(var i=0; i< T_F; i++){
		// Each weight is sequentially smaller
		W[i] = 1.0*(T_F-i)/(T_F);
//		W[i] = 1.0*(1+i)/T_F;
//		W[i] = 1.0;
	}

	// Get original flow vector
	vo = getFlowV(originalseq);

	console.log(vo);
}

// FITNESS

// Get the fitness score for a given sequence
function getFitScore(s){
	return K/(1+K2*getFitSum(s)/T_F);
}

//
// Get weighted sum of the difference between flow vectors
// (Given flow vector vs original flow vector)
function getFitSum(s){
	var vs = getFlowV(s);
	var sum = 0;
	for(var i=0; i<T_F; i++){
		sum += W[i] * abs( vs[i] - vo[i]);
	}
	return sum;
}

function getFlowV(s){
	var v = [];
	for(var i=0; i<T_F; i++){
		v[i] = getSumA(s,i+1)/s.length;
	}

	return v;
}

// Get Sum of the distances between seq[i] and seq[i+a]
function getSumA(s,a){
	var sum = 0;
	for(var i=0; i<s.length-a; i++){
		sum += distances[s[i]][s[i+a]];
	}
	return sum;
}



// DRAWING

function drawSequencePoints(s,x,y,w,h){
	/*
	var min = min(s);
	var max = max(s);
	*/
	var min = 0;
	var max = 50;

	push();
	translate(x,y);
	for(var i=0; i<s.length; i++){
		point(i*w/s.length, h-seq[s[i]]*h/(max-min));

	}
	pop();
}
