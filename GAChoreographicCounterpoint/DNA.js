// Modified by Sejo Vega-Cebrian
// based in:
// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Genetic Algorithm, Evolving Shakespeare

// A class to describe a psuedo-DNA, i.e. genotype
//   Here, a virtual organism's DNA is an array of character.
//   Functionality:
//      -- convert DNA into a string
//      -- calculate DNA's "fitness"
//      -- mate DNA with another set of DNA
//      -- mutate DNA

/* 
 * CONSTANTS
 */
//var T_F = 5;// Number of elements of the Flow Vector
var T_F = seqPopulationNPoses-1;
//var T_F = Math.floor(seqPopulationNPoses*3/4-1);
var W = []; // Weights for 

// Fitness score constants
var K = 100.0; // Maximum value
var K2 = 1.0; // Constant to weigh the FitSum/T_F

function newGene() {

  return floor(random(seqNPoses));
}

// Constructor (makes a random DNA)
function DNA(num) {
  // The genetic sequence
  this.genes = [];
  this.fitness = 0;
  this.flowV = [];

  for (var i = 0; i < num; i++) {
    this.genes[i] = newGene();  // Pick from range of chars
    }

	// Initialize weights
	var p = 5;
	for(var i=0; i< T_F; i++){
		// Each weight is sequentially smaller
//		W[i] = 1.0*(T_F-i)/(T_F);
//		W[i] = 1.0*sq(T_F-i)/sq(T_F);
//		W[i] = 1.0*pow(T_F-i,p)/pow(T_F,p);
//		W[i] = 1.0*(1+i)/T_F;
		W[i] = 1.0;
	}

// FUNCTIONS

  this.setGenes = function(g){
    this.genes = g.slice();
  }

  this.getGenes = function(){
	return this.genes.slice();
  }


  // Custom fitness function 
  this.calcFitness = function(targetDNA) {
//	  this.fitness = K/(1+K2*this.getFitSum(targetDNA)/T_F);
	  // This one preferes non repeated items
	  this.fitness = K/(1+K2*this.getFitSum(targetDNA)/T_F)*(1.0/this.getMaxRepeated());
  }

  // Crossover
  this.crossover = function(partner) {
    // A new child
    var child = new DNA(this.genes.length);
    
    var midpoint;
    if(random(1)<crossoverRate){
      midpoint = floor(random(this.genes.length)); // Pick a midpoint
    }
    else{
      midpoint = (random(1)<0.5) ? 0 : this.genes.length-1;
    }
    
    // Half from one, half from the other
    for (var i = 0; i < this.genes.length; i++) {
      if (i > midpoint) child.genes[i] = this.genes[i];
      else              child.genes[i] = partner.genes[i];
    }
    return child;
  }

  // Based on a mutation probability, picks a new random gene (index)
  this.mutate = function(mutationRate) {
    for (var i = 0; i < this.genes.length; i++) {
      if (random(1) < mutationRate) {
        this.genes[i] = newGene();
      }
    }
  }


 // For custom fitness function
	//
// Get weighted sum of the difference between flow vectors
// (Given flow vector vs original flow vector)
 this.getFitSum = function(targetDNA){
	var vo = targetDNA.getFlowV();
	this.calculateFlowV();
	var sum = 0;
	for(var i=0; i<T_F; i++){
		sum += W[i] * abs( this.flowV[i] - vo[i]);
	}
	return sum;
}

// Return flow vector: vector of sumA with a=1 to T_F
  this.getFlowV = function(){
    return this.flowV;
  }

// Calculate flow vector: vector of sumA with a=1 to T_F
  this.calculateFlowV = function(){
	var v = [];
	for(var i=0; i<T_F; i++){
		v[i] = this.getSumA(i+1)/this.genes.length;
	}

	this.flowV = v; // slice?
  }

// Get sum of the distances between adjacent poses
  this.getSumA = function(a){
	var sum = 0;
	for(var i=0; i<this.genes.length-a; i++){
		sum += distances[this.genes[i]][this.genes[i+a]];
	}
	return sum;
  }

  this.getHistogram = function(){
	var h = [];
	for(var i=0; i<seqNPoses; i++){
		h[i] = 0;
	}
	for(var i=0; i<this.genes.length; i++){
		h[this.genes[i]]++;
	}
//	console.log(h);
	return h;
  }

  // Get maximum number of repetitions of an index
  this.getMaxRepeated = function(){
	  var h = this.getHistogram();
	  return max(h);
  }

}
