// The Nature of Code
// Daniel Shiffman
// http://natureofcode.com

// Genetic Algorithm, Evolving Shakespeare

// Demonstration of using a genetic algorithm to perform a search

// setup()
//  # Step 1: The Population 
//    # Create an empty population (an array or ArrayList)
//    # Fill it with DNA encoded objects (pick random values to start)

// draw()
//  # Step 1: Selection 
//    # Create an empty mating pool (an empty ArrayList)
//    # For every member of the population, evaluate its fitness based on some criteria / function, 
//      and add it to the mating pool in a manner consistant with its fitness, i.e. the more fit it 
//      is the more times it appears in the mating pool, in order to be more likely picked for reproduction.

//  # Step 2: Reproduction Create a new empty population
//    # Fill the new population by executing the following steps:
//       1. Pick two "parent" objects from the mating pool.
//       2. Crossover -- create a "child" object by mating these two parents.
//       3. Mutation -- mutate the child's DNA based on a given probability.
//       4. Add the child object to the new population.
//    # Replace the old population with the new population
//  
//   # Rinse and repeat


var mutationRate = 0.02;    // Mutation rate
var crossoverRate = 0.85;
var totalPopulation = 400;      // Total Population

var population;             // Array to hold the current population
var matingPool;    // ArrayList which we will use for our "mating pool"
var target;                // Target phrase

var display = "";

var generationCount;
var generationPlaying;
var fitnessPlaying;
var generationAbsMax;
var fitnessAbsMax;


// Name of the sequence files to usse
var seqName = "test3"; // test5 or test3
var seqNPoses = 200;
var seqPopulationNPoses = 15;

// Array for the frames
var frames = [];
var framePtr;
// JSON of poses
var poses;
var posesNames = [];

// Stores the distances between poses
var distances = [];

// Original sequence in order
var seqo = [];
var seq2 = [];

var dnao;

function preload(){
	var n;
	// Load poses JSON
	poses = loadJSON("data/"+seqName+".json");


	// Load frames
//	var i=0;
	for(var i=0; i<seqNPoses; i++){
//	for(var pose in poses){
		n = ('0000'+i).slice(-4);
		frames[i] = loadImage("data/frames/"+seqName+"_frame_"+n+".png");
		seqo[i] = i;
		seq2[i] = floor(random(seqNPoses));
	}



}

function setup() {
	
  
  framePtr = 0;
  console.log("Starting at "+hour()+":"+minute());

  console.log("Calculating distances between poses...");
  calculateDistances();
  console.log("Distances calculated!");


  display = createP("");
  display.class("results");
  display.position(10,530);

  var canvas = createCanvas(600, 500);
  canvas.parent("canvashere");
  display.parent("canvashere");

  dnao = new DNA(seqNPoses);
  dnao.setGenes(seqo);
  dnao.calculateFlowV();

  population = [];

  for (var i = 0; i < totalPopulation; i++) {
    population[i] = new DNA(seqPopulationNPoses);
  }

  generationCount = 0;
  generationPlaying = 0;
  fitnessPlaying = 0;

  generationMax = 0;
  fitnessAbsMax = 0;


}

function draw() {
	background(255);




	var maxFitness = 0;
	var minFitness = 100;
	var maxFitnessIndex = 0;
	var avgFitness = 0;

  for (var i = 0; i < population.length; i++) {
    population[i].calcFitness(dnao);
	  // Check for the best fitness
	  if(population[i].fitness>maxFitness){
		maxFitness = population[i].fitness;
		maxFitnessIndex = i;
	  }
	  if(population[i].fitness<minFitness){
		minFitness = population[i].fitness;
	  }

	  avgFitness += population[i].fitness;
  }
  avgFitness /= population.length;

  if(maxFitness>fitnessAbsMax){
	fitnessAbsMax = maxFitness;
	generationMax = generationCount;
  }

	/*
  if(generationCount-generationMax>seqNPoses && frameCount%100 == 0){
		console.log("Increasing mutation rate");
		mutationRate += 0.005;
		console.log(mutationRate);
  }
  */


  var matingPool = [];  // ArrayList which we will use for our "mating pool"

  for (var i = 0; i < population.length; i++) {
    // Arbitrary multiplier, we can also use monte carlo method
      var nnnn = floor(population[i].fitness * 20);  
      for (var j = 0; j <nnnn; j++) {              // and pick two random numbers
        matingPool.push(population[i]);
      }
    }

  // Fill the mating pool with the same population
  // if there's not enough fitness
  if(matingPool.length==0){
    for (var i = 0; i < population.length; i++) {
        matingPool.push(population[i]);
  		}
  }

//	console.log(matingPool.length);

  population[0] = population[maxFitnessIndex];
    population[1] = new DNA(seqPopulationNPoses);

  var child;
  for (var i = 2; i < population.length; i++) {
    var a = floor(random(matingPool.length));
    var b = floor(random(matingPool.length));
    var partnerA = matingPool[a];
    var partnerB = matingPool[b];

    child = partnerA.crossover(partnerB);
    child.mutate(mutationRate);
    population[i] = child;
  }
  
  var everything = "";
	everything += "Generation "+generationCount;
	everything += "<br>";
	everything += "Best score so far: "+maxFitness.toFixed(4);
	everything += "<br>";
	everything += "Playing Best of Generation "+generationPlaying+": "+fitnessPlaying.toFixed(4);
	everything += "<br>";
	everything += "Average score in current Generation: "+avgFitness.toFixed(4);


	drawFrameAndPose(seqo[framePtr],0,0);
	drawFrameAndPose(seq2[framePtr%seqPopulationNPoses],0,240);




	if(framePtr%seqPopulationNPoses == 0){
		if(maxFitness>fitnessPlaying){
			generationPlaying = generationCount;
			fitnessPlaying = maxFitness;
		}

		seq2 = (population[maxFitnessIndex].getGenes());
//console.log(population[maxFitnessIndex].getMaxRepeated());
		/*
		console.log("Best sequence so far");
		console.log(seq2);
		*/
	}

	if(framePtr<seqNPoses-1){
		framePtr++;
	}
	else{
		framePtr = 0;
	}



  textFont("Courier");
  display.html(everything);

	generationCount++;
  //noLoop();
}

// Draw frame with index i along with its pose
function drawFrameAndPose(i,x,y){
	image(frames[i],x,y);
	drawPose(i,x,y,80);
}

// Draw pose skeleton
function drawPose(name,x,y,s){
	var joints = poses[name];

	noFill();
	stroke(255,100);
	rect(x,y,2.5*s,2.5*s);

	push();

	translate(x+s,y+2*s);
	fill(255,0,0);
	noStroke();
	scale(s);
	for(var j in joints){
		ellipse(joints[j].x,-joints[j].y,0.1,0.1);
	}
	pop();
}


// Create table of distances between poses
function calculateDistances(){
	var dist;
	var j1, j2;
	var v1, v2;
	// For each pose
	for(var pose in poses){
//		console.log("Distances from "+pose);
		distances[pose] = [];
		// Check every other pose
		for(var pose2 in poses){
//			dist = distanceBetweenPoses(pose,pose2);
			dist = alternativeDistanceBetweenPoses(pose,pose2);
			distances[pose][pose2] = dist;
			

		}
	}

//	console.log(distances);

}

// Sum of euclidean distances of joints
function distanceBetweenPoses(pose, pose2){
	dist = 0;
	// And measure distance for each joint
	for(var joint in poses[pose]){
		j1 = poses[pose][joint];
		j2 = poses[pose2][joint];
		v1 = createVector(j1.x,j1.y,j1.z);
		v2 = createVector(j2.x,j2.y,j2.z);


		dist += p5.Vector.dist(v1,v2);
	}

	return dist;
}

// Euclidean distance between two poses
function alternativeDistanceBetweenPoses(pose, pose2){
	var deltas = [];
	// And measure distance for each joint
	for(var joint in poses[pose]){
		j1 = poses[pose][joint];
		j2 = poses[pose2][joint];

		deltas.push(j1.x-j2.x);
		deltas.push(j1.y-j2.y);
		deltas.push(j1.z-j2.z);
	}

	var dist = 0;
	for(var i=0; i<deltas.length;i++){
		dist += sq(deltas[i]);
	}
	dist = sqrt(dist);

	return dist;
}

